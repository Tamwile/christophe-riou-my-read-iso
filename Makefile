
CC=gcc

CFLAGS= -Wall -Werror -Wextra -pedantic -std=c99

all: my_read_iso.c
	${CC} ${CFLAGS} my_read_iso.c -o my_read_iso

test: my_read_iso
	./test_my_read_iso.sh

clean:
	${RM} *.o my_read_iso


