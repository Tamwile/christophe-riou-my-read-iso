#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>


#include "iso9660.h"
#include "my_read_iso.h"

#define SUPER_BLOCK_OFFSET (ISO_PRIM_VOLDESC_BLOCK * ISO_BLOCK_SIZE)


/* int read_cmd_file(const char *pathname) */
/* { */
/*   // fopen("./commands"); ... */
/*   return 0; */
/* } */


int main(int argc, char** argv)
{
  int len_argv0 = strlen(argv[0])-2;

  if (argc != 2)
  {
    fprintf(stderr, "usage: %s FILE\n",  argv[0]);
    return -1;
  }
  
  int fd = open(argv[1], O_RDONLY);
  if(fd < 0)
  {
    fprintf(stderr, "fail to open: %s\n", argv[1]);
    return -1;
  }

  struct stat stat;

  if (fstat(fd, &stat) < 0)
  {
    fprintf(stderr, "%.*s: %s: invalid ISO9660 file\n", len_argv0, argv[0]+2, argv[1]);
    return -1;
  }

  unsigned int size = stat.st_size;

  if (size < sizeof(struct iso_prim_voldesc))
  {
    fprintf(stderr, "%.*s: %s: invalid ISO9660 file\n", len_argv0, argv[0]+2, argv[1]);
    return -1;
  }

  void *ptr = mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  if (ptr == MAP_FAILED)
  {
    fprintf(stderr, "fail to map data\n");
    return -1;
  }

  if (close(fd) < 0)
  {
    fprintf(stderr, "fail to close file\n");
    return -1;
  }

  struct iso_prim_voldesc *super = iso_get_super_block(ptr);

  // printf("super->std_identifier : %.*s\n", 5, super->std_identifier);

  if (strncmp(super->std_identifier, "CD001", 5)  != 0)
  {
    fprintf(stderr, "%.*s: %s: invalid ISO9660 file\n", len_argv0, argv[0]+2, argv[1]);
    return -1;
  }

  
  // shell();

  //shell_info(super);
  //shell_help();

  
  struct iso_dir *root =  &(super->root_dir);
  // printf("root addr: %p", root);
  printf("dir_size: %d\n", root->dir_size);
  printf("ext_size: %d\n", root->ext_size);
  printf("data_blk: %d\n", root->data_blk.le);
  printf("file_size: %d\n", root->file_size.le);
  printf("date: %s", root->date);
  printf("type: %d\n", root->type);
  printf("unit_size: %d\n", root->unit_size);
  printf("gap_size: %d\n", root->gap_size);
  printf("idf_len: %d\n", (root->idf_len));
  printf("root: %d\n", (root + root->dir_size)->idf_len);
  printf("root+1: %d\n", (root + root->dir_size + sizeof(char))->idf_len);
  printf("root+2: %d\n", (root+2)->idf_len);
  printf("sizeof char %ld\n", sizeof(uint8_t));

  
  void *tmp = root + root->dir_size + 1;
  char *name = tmp;
  int len  = root->idf_len;
  
  if (*name == 0)
  {
    name = ".";
    len = 1;
  }
  else if (*name == 1)
  {
    name = "..";
    len = 2;
  }
  len = len;
  printf("filename: %.*s\n", root->idf_len, name);

  // do {
  // write(1, "> ", 3);
  // while ()
  // }
  
  if (munmap(ptr, stat.st_size) < 0)
  {
    fprintf(stderr, "fail to unmap data\n");
    return -1;
  }
}



void *iso_get_super_block(void *image)
{
  char *ptr = image;
  return ptr + SUPER_BLOCK_OFFSET;
}

void shell_info(struct iso_prim_voldesc *super)
{
  printf("System Identifier: %.*s\n", ISO_SYSIDF_LEN, super->syidf);
  printf("Volume Identifier: %.*s\n", ISO_VOLIDF_LEN, super->vol_idf);
  printf("Block count: %d\n", super->vol_blk_count.le);
  printf("Block size: %d\n", super->vol_blk_size.le);
  printf("Creation date: %.*s\n", ISO_LDATE_LEN, super->date_creat);
  printf("Application Identifier: %.*s\n", ISO_APP_LEN, super->app_idf);
}


void shell_help(void)
{
  puts("help: display command help");
  puts("info: display volume info");
  puts("ls: display the content of a directory");
  puts("cd: change current directory");
  puts("tree: display the tree of a directory");
  puts("get: copy file to local directory");
  puts("cat: display file content");
  puts("pwd: print current path");
  puts("quit: program exit");
}

/* void shell_ls(void) */
/* { */
/*   return 0; */
/* } */


